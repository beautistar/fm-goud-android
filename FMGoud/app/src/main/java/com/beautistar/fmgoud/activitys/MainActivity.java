package com.beautistar.fmgoud.activitys;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.extractor.ExtractorSampleSource;
import com.google.android.exoplayer.upstream.Allocator;
import com.google.android.exoplayer.upstream.DataSource;
import com.google.android.exoplayer.upstream.DefaultAllocator;
import com.google.android.exoplayer.upstream.DefaultUriDataSource;
import com.google.android.exoplayer.util.Util;
import com.hanks.htextview.typer.TyperTextView;
import com.beautistar.fmgoud.R;
import com.beautistar.fmgoud.base.CommonActivity;
import com.beautistar.fmgoud.commons.Constants;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends CommonActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawer_menu) NavigationView ui_drawer_menu;
    @BindView(R.id.drawerlayout) DrawerLayout ui_drawerlayout;

    @BindView(R.id.toolbar_menu) Toolbar toolbar_menu;
    @BindView(R.id.imv_menu) ImageView imv_menu;
    @BindView(R.id.imv_info) ImageView imv_info;

    @BindView(R.id.rlt_live) RelativeLayout rlt_live;
    @BindView(R.id.rlt_tvgroud) RelativeLayout rlt_tvgroud;
    @BindView(R.id.rlt_over) RelativeLayout rlt_over;
    @BindView(R.id.txt_typer) TyperTextView txt_typer;

//    @BindView(R.id.player) EasyVideoPlayer player;

    MediaPlayer live_mp;
    MediaPlayer tv_mp;

    Timer livetimer, tvTimer;

    View header_view;
    ActionBarDrawerToggle drawerToggle;
    ExoPlayer exoPlayer_live_mp;
    ExoPlayer exoPlayer_tv_mp;
    private static final int BUFFER_SEGMENT_SIZE = 64 * 1024;
    private static final int BUFFER_SEGMENT_COUNT = 256;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        loadLayout();
    }

    private void loadLayout() {


        ui_drawer_menu.setNavigationItemSelectedListener(this);
        header_view = ui_drawer_menu.getHeaderView(0);
        ui_drawerlayout.addDrawerListener(drawerToggle);

       /* live_mp = new MediaPlayer();
        live_mp.setScreenOnWhilePlaying(true);

        tv_mp = new MediaPlayer();
        tv_mp.setScreenOnWhilePlaying(true);*/

        setupNavigationBar();
        txt_typer.setText("");

        exoPlayer_live_mp = ExoPlayer.Factory.newInstance(1);
        exoPlayer_tv_mp = ExoPlayer.Factory.newInstance(1);

    }

    public void exoPlayer_LIVE (String radioChannelUrl){

        Uri radioUri = Uri.parse(radioChannelUrl);

        // Settings for exoPlayer
        Allocator allocator = new DefaultAllocator(BUFFER_SEGMENT_SIZE);
        String userAgent = Util.getUserAgent(this, "ExoPlayerDemo");
        DataSource dataSource = new DefaultUriDataSource(this, null, userAgent);
        ExtractorSampleSource sampleSource = new ExtractorSampleSource(
                radioUri, dataSource, allocator, BUFFER_SEGMENT_SIZE * BUFFER_SEGMENT_COUNT);
        MediaCodecAudioTrackRenderer audioRenderer = new MediaCodecAudioTrackRenderer(sampleSource);
        // Prepare ExoPlayer
        exoPlayer_live_mp.prepare(audioRenderer);

        closeProgress();
        exoPlayer_live_mp.setPlayWhenReady(true);
        txt_typer.animateText(getString(R.string.playing_groud));

       /* if (exoPlayer_live_mp != null && exoPlayer.getPlayWhenReady()) {

            closeProgress();

            exoPlayer.stop();
            //ui_imvPlay.setImageResource(R.drawable.ic_play);
            exoPlayer.setPlayWhenReady(false);

        }else {
            closeProgress();
            exoPlayer.setPlayWhenReady(true);
            //ui_imvPlay.setImageResource(R.drawable.ic_pause);
        }*/
    }

    public void exoPlayer_TV(String radioChannelUrl){

        Uri radioUri = Uri.parse(radioChannelUrl);

        // Settings for exoPlayer
        Allocator allocator = new DefaultAllocator(BUFFER_SEGMENT_SIZE);
        String userAgent = Util.getUserAgent(this, "ExoPlayerDemo");
        DataSource dataSource = new DefaultUriDataSource(this, null, userAgent);
        ExtractorSampleSource sampleSource = new ExtractorSampleSource(
                radioUri, dataSource, allocator, BUFFER_SEGMENT_SIZE * BUFFER_SEGMENT_COUNT);
        MediaCodecAudioTrackRenderer audioRenderer = new MediaCodecAudioTrackRenderer(sampleSource);
        // Prepare ExoPlayer
        exoPlayer_tv_mp.prepare(audioRenderer);

        closeProgress();
        exoPlayer_tv_mp.setPlayWhenReady(true);
        txt_typer.animateText(getString(R.string.playing_groud_plus));
    }

    private void setupNavigationBar() {

        drawerToggle = new ActionBarDrawerToggle(this, ui_drawerlayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);}

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);}
        };

        ui_drawerlayout.setDrawerListener(drawerToggle);

    }



    @OnClick(R.id.rlt_live) void gotoFMGroud(){

        if (livetimer!=null) {
            livetimer.cancel();
        }

        if (tvTimer!=null) {
            tvTimer.cancel();
        }

        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {

                _handler.post(new Runnable() {
                    @Override
                    public void run() {

                        txt_typer.animateText(getString(R.string.playing_groud));

                    }
                });
            }
        };

        livetimer = new Timer();

        livetimer.schedule(doAsynchronousTask, 0, 10000);

        showProgress();

        if (exoPlayer_tv_mp.getPlayWhenReady() && exoPlayer_tv_mp != null) {
            exoPlayer_tv_mp.stop();
            exoPlayer_tv_mp.setPlayWhenReady(false);
        }

       /* try {

            live_mp.setDataSource(getString(R.string.live));
            txt_typer.animateText(getString(R.string.playing_groud));
           // txt_typer.setTyperSpeed(10);
            live_mp.prepare();

        } catch (IOException e) {

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        live_mp.start();

        if (live_mp.isPlaying()) {
            closeProgress();
        }*/

       exoPlayer_LIVE(getString(R.string.live));
    }

    @OnClick(R.id.rlt_tvgroud) void gotoTVGroud() {

        if (livetimer!=null) {
            livetimer.cancel();
        }

        if (tvTimer!=null) {
            tvTimer.cancel();
        }

        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {

                _handler.post(new Runnable() {
                    @Override
                    public void run() {

                        txt_typer.animateText(getString(R.string.playing_groud_plus));

                    }
                });
            }
        };

        livetimer = new Timer();
        livetimer.schedule(doAsynchronousTask, 0, 10000);


        showProgress();

        if (exoPlayer_live_mp.getPlayWhenReady() && exoPlayer_live_mp != null) {
            exoPlayer_live_mp.stop();
            exoPlayer_live_mp.setPlayWhenReady(false);
        }

       /* try {

            tv_mp.setDataSource(getString(R.string.groud));
            txt_typer.setCharIncrease(10);
            txt_typer.animateText(getString(R.string.playing_groud_plus));
            //txt_typer.setTyperSpeed(10);
            tv_mp.prepare();

        } catch (IOException e) {

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        tv_mp.start();

        if (tv_mp.isPlaying()) {
            closeProgress();
        }*/

       exoPlayer_TV(getString(R.string.groud));

    }

    @OnClick(R.id.imv_menu) void call_menu() {

        ui_drawerlayout.openDrawer(Gravity.LEFT);
    }

    @OnClick(R.id.rlt_over) void exit(){

        exoPlayer_tv_mp.stop();
        exoPlayer_tv_mp.setPlayWhenReady(false);
        exoPlayer_live_mp.stop();
        exoPlayer_live_mp.setPlayWhenReady(false);
        finish();
    }

    @Override
    public void onBackPressed() {

        exoPlayer_tv_mp.stop();
        exoPlayer_tv_mp.setPlayWhenReady(false);
        exoPlayer_live_mp.stop();
        exoPlayer_live_mp.setPlayWhenReady(false);

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        ui_drawerlayout.closeDrawers();

        switch (id){

            case R.id.nav_over_ons:
                showAlertDialog(getString(R.string.alert));
                break;
        }

        return false;
    }
}
