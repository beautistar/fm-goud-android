package com.beautistar.fmgoud.activitys;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


import com.beautistar.fmgoud.R;
import com.beautistar.fmgoud.base.CommonActivity;
import com.beautistar.fmgoud.commons.Constants;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FMGoudActivity extends CommonActivity {


    boolean isPLAYING, isPlayVideo;
    MediaPlayer mp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fmgoud);
        ButterKnife.bind(this);

        loadLayout();


    }

    private void loadLayout() {

        mp = new MediaPlayer();
        mp.setScreenOnWhilePlaying(true);
        try {
            mp.setDataSource(getString(R.string.live));
            mp.prepare();

        } catch (IOException e) {

        }

        playVideo();
    }

    private void playVideo() {

        if (!Constants.LIVE_TUNR) {

            Log.e("LOG:", "prepare() play");
            Constants.LIVE_TUNR = true;
            mp.start();

        } else {
            Constants.LIVE_TUNR = false;
            stopPlaying();
            Log.e("LOG:", "prepare() stop");

        }
    }

    private void stopPlaying() {

        //visualView.setVisibility(View.GONE);
        mp.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mp.pause();

        Log.d("aaaaaaaaaaa", "bbbbbbbbbbbbb0");
    }
}
