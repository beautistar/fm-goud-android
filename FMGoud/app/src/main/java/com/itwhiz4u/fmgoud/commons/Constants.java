package com.itwhiz4u.fmgoud.commons;

import java.util.ArrayList;

/**
 * Created by Snowflake on 7/27/2016.
 */
public class Constants {

    public static final int SPLASH_TIME = 2000;
    public static final int PROFILE_IMAGE_SIZE = 256;
    public static final int VOLLEY_TIME_OUT = 60000;

    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;
    public static final int PICK_FROM_VIDEO = 104;
    public static final int REQUEST_CODE = 200;

    public static final String KEY_LOGOUT = "logout";

    public static final String FM_GROUD_CHANNEL = "http://stream01.level27.be:8008";
    public static final String FM_GROUDPLUS_CHANNEL = "http://stream01.level27.be:8010";

    public static boolean LIVE_TUNR = false;
    public static boolean TV_TURN = false;

}
