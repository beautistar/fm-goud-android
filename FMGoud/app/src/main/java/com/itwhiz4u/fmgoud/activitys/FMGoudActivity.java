package com.itwhiz4u.fmgoud.activitys;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.itwhiz4u.fmgoud.R;
import com.itwhiz4u.fmgoud.base.CommonActivity;
import com.itwhiz4u.fmgoud.commons.Constants;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FMGoudActivity extends CommonActivity implements EasyVideoCallback{

    @BindView(R.id.player) EasyVideoPlayer player;

    boolean isPLAYING, isPlayVideo;
    MediaPlayer mp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fmgoud);
        ButterKnife.bind(this);

        loadLayout();


    }

    private void loadLayout() {

        mp = new MediaPlayer();
        mp.setScreenOnWhilePlaying(true);
        try {
            mp.setDataSource(getString(R.string.live));
            mp.prepare();

        } catch (IOException e) {

        }

        playVideo();
    }

    private void playVideo() {

        if (!Constants.LIVE_TUNR) {

            Log.e("LOG:", "prepare() play");
            Constants.LIVE_TUNR = true;
            mp.start();

        } else {
            Constants.LIVE_TUNR = false;
            stopPlaying();
            Log.e("LOG:", "prepare() stop");

        }
    }

    private void stopPlaying() {

        //visualView.setVisibility(View.GONE);
        mp.pause();
    }

    @Override
    public void onStarted(EasyVideoPlayer player) {


    }

    @Override
    public void onPaused(EasyVideoPlayer player) {

    }

    @Override
    public void onPreparing(EasyVideoPlayer player) {

    }

    @Override
    public void onPrepared(EasyVideoPlayer player) {

    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(EasyVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(EasyVideoPlayer player) {

    }

    @Override
    public void onRetry(EasyVideoPlayer player, Uri source) {

        player.setAutoPlay(true);
    }

    @Override
    public void onSubmit(EasyVideoPlayer player, Uri source) {

        player.pause();
        player.setVisibility(View.GONE);
    }

    @Override
    public void onClickVideoFrame(EasyVideoPlayer player) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mp.pause();

        Log.d("aaaaaaaaaaa", "bbbbbbbbbbbbb0");
    }
}
